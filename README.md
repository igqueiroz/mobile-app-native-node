# NodeJs Serverless AppService #

This is a Heroku Backend made for React Native App

### Stack ###

* MySQL 8
* Sequelize 6.9
* Jest
* Express
* JWT
* Node Mailer
* [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
* [React Native Code](https://bitbucket.org/igqueiroz/react-mobile-app)

### Activation Page ###

[PHP Activation Page](https://bitbucket.org/igqueiroz/mobile-app-native-node/src/main/activation/index.php)

### Database Schema ###

[Dump](https://www.igorqueiroz.com.br/activation/files/prchlik_iad1-mysql-e2-13b_dreamhost_com.sql)

![Postgres 13](./database.png?raw=true "Postgres 13 - Relations")

### App Development Mode APK ###

[APK pre-release v0](https://www.igorqueiroz.com.br/activation/files/a9f16902-d620-4665-a0a6-85f0050e8261-498955c8158d4903af794da66f50edb5.apk)

### Commands ###

1) ```npm run test:watch``` Watch Jest Tests

1) ```npm run deploy:local``` Deploy Local Heroku

1) ```npm run deploy:heroku``` Deploy Production Heroku (main branch)

1) ```npm run logs:heorku``` Watch Heroku Logs
