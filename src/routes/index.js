const route = require('express').Router()
const { 
    users,
    token
    } = require('../controllers')
const Cache = require('../utils/express-tools')
const cors = require('cors')

const serverTools = {
    cors: Cache.allowedOrigins(),
    headers: Cache.setCacheHeaders(['private', 'max-age=3600']),
    recaptcha: Cache.recaptcha
}

// route.get('/users',  cors(serverTools.cors), serverTools.headers, serverTools.recaptcha, users.getAll.bind(users))
route.get('/users',  users.getAll.bind(users)) // list 
route.post('/user', users.create.bind(users)) // signup
route.get('/user/:id', users.getOne.bind(users)) // get id info
route.delete('/user/:id', users.deleteOne.bind(users)) // remove

route.post('/login', users.login.bind(users)) // signin

route.get('/token/verify/:type/:id/:doc', token.verifyAccount.bind(token)) // verify account email/sms
route.get('/token/:id', token.getOne.bind(token)) // get token info
route.get('/token/activation/:id/:email64', token.updateToken.bind(token)) // resend email with new token
module.exports = route