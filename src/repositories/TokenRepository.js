const db = require('../models')

const error = (e) => {
    let errorMsg = {
        success: false,
        message: e.message || "",
        query: e.sql || "", 
        details: e.original && e.original.sqlMessage || "",
        errorsLength: e.errors && e.errors.length || "",
        code: e.original && e.original.code || ""
    }
    console.error(errorMsg)
    return errorMsg
}

const getOne = async (type, value) => {
    try {
        const user = await db.models.token.findOne({ where: { userid: value } })
        if (user === null) return []
        return user.dataValues
    } catch (e) {
        return error(e)
    }
}

const updateOne = async (data, id) => {
    try {
        const update = await db.models.token.update(data, { where: { userid: id } })
        if (update === null) return []
        return update
    } catch (e) {
        return error(e)
    }
}


module.exports = { getOne, updateOne }