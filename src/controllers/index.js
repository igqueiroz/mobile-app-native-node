const users = require('./users')
const token = require('./token')
const email = require('./email')

module.exports = { 
    users,
    token,
    email
}