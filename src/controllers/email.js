const nodemailer = require("nodemailer")
const user = require('./users')

class Email {
    constructor() {
        this.transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: 465,
            secure: true,
            auth: {
              user: process.env.MAIL_USER,
              pass: process.env.MAIL_PASS,
            }
          });
    }

    async send (to, subject, text, req = null, res = null) {
      if (req && res) {
        const { id } = req.params
        const validUser = await user.getOne(id)
        console.log(validUser, validUser)

      }
      let infoData = await this.transporterData(to, subject, text)
      if (infoData && req && res) {
        return (infoData.messageId) ? res.status(200).send({ success: true, msg: infoData.messageId }) : res.status(500).send({ success: false, msg: infoData })
      }
      return (infoData.messageId) ? { success: true } : { success: false }
    }

    async transporterData(to, subject, text) {
      let result = await this.transporter.sendMail({from: process.env.MAIL_USER, to, subject, text })
      return result
    }

}

module.exports = new Email()

