const token = require('../repositories/TokenRepository')
const utils = require('../utils/object')
const moment = require('moment')
const { v4: uuidv4 } = require('uuid')
const email = require('./email')
const user = require('./users')

class Token {
    constructor() {
    }

    async create(id) {
        const result = await token.create(id)
        if (result.errors && result.errors > 0) return result
        return result
    }

    async getOne(req, res, idOrEmail = null) {
        const value = req && req.params.id || idOrEmail
        const type = utils.findValueType(value)
        if (!type) return res.status(400).send('bad format!')
        
        const result = await token.getOne(type, value)
        if (result.errors && result.errors > 0) {
            if (res) return res.status(400).send(result)
            else return []
        }
        if (res) return res.status(200).send(result)
        else return result
    }

    async verifyAccount(req, res) {
        const { doc, id, type } = req.params
        let idToken = await this.getOne(null, null, id)
        // verify user
        if (idToken.length == 0) return res.status(404).send({ success: false, msg: "User not found." })
        // verify token
        if (doc !== idToken[type]) return res.status(404).send({ success: false, msg: "Invalid Token" })
        // verify token expiration
        if ( moment(idToken.expire_at).toDate() < moment() ) return res.status(404).send({ success: false, msg: "Token expired" })
        // change user status to verified
        const result = user.updateOne( { is_verified: 1 }, id )
        return res.status(200).send({result})
    }

    async updateToken(req, res) {
        const { id, email64 } = req.params
        try {
            const verifyToken = {
                sms: (Math.floor((Math.random() * 1000000) + 1)).toString().padEnd(6, "0"),
                email: uuidv4(),
                expire_at: moment(new Date()).add(5, 'm').toDate() // restart time to next 5m
            }
            const result = await token.updateOne(verifyToken, id)
            if (result > 0) {
                const userInfo = await token.getOne('email', id)
                const buff = Buffer.from(email64, 'base64')
                const emailDecoded = buff.toString('utf-8')
                const emailSend = await email.send(emailDecoded, 'App - E-mail de Ativação - Reenvio', `https://igorqueiroz.com.br/activation/?a=${userInfo.email}-i${userInfo.userid}`)
                return (emailSend.success) ? res.status(200).send(emailSend) : res.status(500).send(emailSend)
            }
            return res.status(500).send({ success: false, message: "ID not found" })
        } catch (e) {
            console.error(e);
            return res.status(500).send({ success: false })
             
        }
    }
}

module.exports = new Token()