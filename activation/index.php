<?php
    $url_param = $_GET['a'];
    $get_last_param = explode('-', $url_param);
    $get_user_id = explode('i', end($get_last_param));
    $user_id = $get_user_id[1]; // 78

    array_pop($get_last_param);
    $uuid = join('-', $get_last_param); // bf86c7a6-7bf3-4965-a302-6cdb857d785b
    $host = "https://mobile-app-native-mode.herokuapp.com/react-native-backend/v0";
    $path_verify = "/token/verify/email/" . $user_id . "/" . $uuid;
    $message = "";
    $result = "";

    $curl_data = fetchData($host, $path_verify);
    if ($curl_data->httpcode == "200") {
        $message = "Sua conta foi ativada com sucesso! Acesse novamente seu app e faça o login.";
    } else {
        if ($curl_data->httpcode == "404") {
            $message = "Desculpe esse link expirou.<br/>Volte no aplicativo e clique em login para reenviar o email de ativação.";
        } else {
            $message = $result . ' - <em>EC' . $curl_data->httpcode . '</em>.';
        }
    };

    function fetchData($host, $path) {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host . $path);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($server_output->errors) throw new \Exception('Erro de comunicação, tente recarregar essa página', 500);
            $result = json_decode($server_output);
            $result->httpcode = $httpcode;
            curl_close ($ch);
            return $result;
        } catch (\Exception $e) {
            $curl_result = new stdClass();
            $curl_result->success = false;
            $curl_result->message = $e->getMessage(); 
            $curl_result->httpcode = $e->getCode();
            return $curl_result;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Igor Queiroz App - Link de Ativação</title>
</head>
<style>
    .body-position {
        display: flex;

    }
</style>
<body>
    <div class="body-position">
        <div class="box-wrapper">
            <h1>Seu Link de Ativação</h1>
            <hr />
            <p>
                <?php echo($message); ?> 
            </p>
        </div>
    </div>
</body>
</html>


<?php
?>